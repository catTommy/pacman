﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using Fusion;
using Fusion.Audio;
using Fusion.Content;
using Fusion.Graphics;
using Fusion.Input;
using Fusion.Utils;
using Fusion.UserInterface;
using System.ComponentModel;
using System.IO;
using System.Threading;

namespace Pacman{

	
	
	class Logic : GameService{
		public static List<Food> foods;
		List<Wall> walls;
		PacmanHero p;
		List<Enemy> enemies;
		string[] strings;
		SpriteFont	font1;
        int points;
        Enemy monster = new Enemy();

       public static Texture2D gameover;

        TimerCallback tm = new TimerCallback(Enemy.toMove);
        Timer timer = new Timer(Enemy.toMove, Enemy.enemyDirec, 5000, 100); 
       

		public SoundEffectInstance music;


		public static int [,] field;
        public static int cellSize;

		public Logic(Game game)
            : base(game)
        {
           
        }

		 public override void Initialize()
        {
            base.Initialize();

			p		= new PacmanHero();
			foods	= new List<Food>();
			enemies = new List<Enemy>();
			walls	= new List<Wall>();


           //  private TimeSpan tmr = new TimeSpan(3000);


			cellSize = 30;
            points = 0;

            
			
			p.pacman			= Game.Content.Load<Texture2D>("pacman-animation");
			p.pacmanLeft		= Game.Content.Load<Texture2D>("pacman-animation4");
			p.pacmanRight		= Game.Content.Load<Texture2D>("pacman-animation");

            

			Food.food			= Game.Content.Load<Texture2D>("burger-icon");

			Enemy.enemyRed		= Game.Content.Load<Texture2D>("pacmanRed");
			Enemy.enemyBlue		= Game.Content.Load<Texture2D>("pacmanBlue");
			Enemy.enemyMagenta	= Game.Content.Load<Texture2D>("pacmanMagenta");

			Wall.wall			= Game.Content.Load<Texture2D>("i");

            //gameover = Game.Content.Load<Texture2D>("LOGOBIG");

			font1	=	Game.Content.Load<SpriteFont>("segoe80");

			music = Game.Content.Load<SoundEffect>("pacman1").CreateInstance();
					
			//считывание карты из файла
			strings = File.ReadAllLines("map1.txt");
			field = new int[strings.Length, strings[0].Length / 2 + 1];
			for (int i = 0; i < field.GetLength(0); i++){
                for (int j = 0; j < field.GetLength(1); j++){
                    field[i, j] = int.Parse(strings[i].Split(' ')[j]);
				}
			}

			 //инициализация карты
			 for (int i = 0; i < field.GetLength(0); i++){
			 	for (int j = 0; j < field.GetLength(1); j++){
				   
					
					switch (field[i,j])
				   {
				   	case 0:
				   		var food = new Food(i*30,j*30);
				   		foods.Add(food);
				   		continue;
				   	  case 1:
				   		var wall = new Wall(i*30,j*30);
				   		walls.Add(wall);
				   		continue;
						case 2:
						p.y = i*30;
						p.x = j*30;
				   		continue;
				   		case 3:
                        //var enemy = new Enemy(i*30,j*30);
                        //enemies.Add(enemy);
                        Enemy.y = i * 30;
                        Enemy.x = j * 30;
				   		continue;
				   }
			 	}
			 }
			music.Volume = 0.4f;
			music.Play(true);
            
        }

         

         public int pacmanField()
         {
             return field[p.y / cellSize, p.x / cellSize];
         }

         public bool isWallUp()
         {
             bool tmp = field[(p.y - p.step) / p.step, p.x / p.step] == 1;
             return tmp;
         }

         public bool isWallDown()
         {
             return field[(p.y + p.step) / p.step, p.x / p.step] == 1;
         }

         public bool isWallRight()
         {
             return field[p.y / p.step, (p.x + p.step) / p.step] == 1;
         }

         public bool isWallLeft()
         {
             return field[p.y / p.step, (p.x - p.step) / p.step] == 1;
         }


         public bool isFood()
         {
             return pacmanField() == 0;
         }


         public bool isEnemy()
         {
             return pacmanField() == 3;
         }

         

		public override void Update(GameTime gameTime)
		{
			base.Update(gameTime);

           

			 //int moveXLeft = p.x - p.step;
			 //int moveXRight = p.x + p.step;
			 //int moveYUp = p.x + p.step;
			 //int moveYDown = p.x - p.step;

			 //int nextUp = field[(p.y + p.step) / p.step, p.x / p.step];
			 //int nextDown = field[(p.y - p.step) / p.step, p.x / p.step];
			 //int nextRight = field[(p.y / p.step), (p.x + p.step) / p.step];
			 //int nextLeft = field[(p.y + p.step) / p.step, (p.x - p.step) / p.step];

			if(p.isAlive){
                 p.Moving(Game.InputDevice);
            
            }



            Console.WriteLine(Enemy.x + "X");
            Console.WriteLine(Enemy.y + "Y");

             //Console.WriteLine(p.x + "pc x");
             //Console.WriteLine(p.y + "pc y");

			if (isFood())
				{
					foods.RemoveAll(n => n.x == p.x && n.y == p.y);
                  //  points++;
                }


                 //if (Enemy.enemyDirection == Direction.UP)
                 //{
                 //    Enemy.isMoovingUp = Enemy.isWallUp() ? false : true;
                 //}

                 //if (Enemy.enemyDirection == Direction.DOWN)
                 //{
                 //    Enemy.isMoovingDown = Enemy.isWallDown() ? false : true;
                 //}
                 //if (Enemy.enemyDirection == Direction.LEFT)
                 //{
                 //    Enemy.isMoovingLeft = Enemy.isWallLeft() ? false : true;
                 //}

                 //if (Enemy.enemyDirection == Direction.RIGHT)
                 //{
                 //    Enemy.isMoovingRight = Enemy.isWallRight() ? false : true;
                 //}



                 if (p.pacmanDirection == Direction.UP)
                 {
                     p.isMoovingUp = isWallUp() ? false : true;
                 }

                 if (p.pacmanDirection == Direction.DOWN)
                 {
                     p.isMoovingDown = isWallDown() ? false : true;
                 }
                 if (p.pacmanDirection == Direction.LEFT)
                 {
                     p.isMoovingLeft = isWallLeft() ? false : true;
                 }

                 if (p.pacmanDirection == Direction.RIGHT)
                 {
                     p.isMoovingRight = isWallRight() ? false : true;
                 }
                 
            //foreach (var e in enemies)
            //{
            //    if (((Math.Abs(p.y - e.y)) < 40) && ((Math.Abs(p.x - e.x)) < 40))
            //    {
            //        p.isAlive = false;
            //        //вывод гейм овер
            //    } 
            //}

            if (((Math.Abs(p.y - Enemy.y)) < 40) && ((Math.Abs(p.x - Enemy.x)) < 40))
            {
                p.isAlive = false;
                //вывод гейм овер
            } 

         }


		public override void Draw(GameTime gameTime, StereoEye stereoEye)
        {
            var sb = Game.GetService<SpriteBatch>();

            

            sb.Begin();

			

			foreach (var f in foods)
			{
			f.Draw(sb);
			}

			foreach (var w in walls)
			{
			w.Draw(sb);
			}

            //foreach (var e in enemies)
            //{
            //e.Draw(sb);
            //}

            monster.Draw(sb);

            //foreach (var e in enemies)
            //{
            //    e.toMove();
            //}

        //    Enemy.toMove(1);

		//    font1.DrawString( sb, string.Format("{1} sprites -> FPS = {0}", gameTime.Fps, points), 64, 256, Color.White );

          //  Console.WriteLine(Enemy.enemyDirection);


            if (p.isAlive)
            {
                p.Draw(sb);
            }
            else
            {
                //sb.DrawSprite(gameover, 300, 300, gameover.Width, gameover.Height, 0, Color.White);
                font1.DrawString(sb, string.Format("GAME OVER"),(Game.Parameters.Width-400)/2, Game.Parameters.Height/2, Color.White);

            }

            sb.End();

            
            
            base.Draw(gameTime, stereoEye);
            
        }




	}
}
