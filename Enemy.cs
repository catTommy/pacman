﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using Fusion;
using Fusion.Audio;
using Fusion.Content;
using Fusion.Graphics;
using Fusion.Input;
using Fusion.Utils;
using Fusion.UserInterface;
using System.ComponentModel;
using System.Threading;

namespace Pacman{


	// для врагов разного цвета
	//public enum enemyType
	//{
	//	RED = 25,
	//	BLUE ,
	//	MAGENTA ,
	//}

	public class Enemy { 


		public static Texture2D enemyRed;
        public static Texture2D enemyBlue;
        public static Texture2D enemyMagenta;
		public static Texture2D enemytexture;
     //   private Timer tmr = new Timer(new TimerCallback(toMove, ene));
        
        
     //   private TimeSpan tmr = new TimeSpan(3000);

		//public static enemyType type; 


        public bool isAliveRed = true;
        public bool isAliveBlue = true;
        public bool isAliveMagenta = true;

        public static bool isMoovingUp = true;
        public static bool isMoovingDown = true;
        public static bool isMoovingLeft = true;
        public static bool isMoovingRight = true;

        private static int step = 30;
        private static int step1 = 30;

        public static  Direction enemyDirection;
        public static int enemyDirec = 0;

        

        public static Random rand = new Random();
        

		public static int x;
        public static int y;


		public float a = 0;

        public Enemy(int enemyY, int enemyX)

        {
			x = enemyX;
			y = enemyY;
        }

        public Enemy()
        {


        }


        public static void toMove(object obj)
        {
           enemyDirection = (Direction)rand.Next(0, 4);


            Console.WriteLine(enemyDirection);

            //if (enemyDirection == Direction.UP)
            //{
            //    isMoovingUp = isWallUp() ? false : true;
            //}

            //if (enemyDirection == Direction.DOWN)
            //{
            //    isMoovingDown = isWallDown() ? false : true;
            //}
            //if (enemyDirection == Direction.LEFT)
            //{
            //    isMoovingLeft = isWallLeft() ? false : true;
            //}

            //if (enemyDirection == Direction.RIGHT)
            //{
            //    isMoovingRight = isWallRight() ? false : true;
            //}
            for (int i = 0; i < rand.Next(6); i++)
            {
                switch (enemyDirection)
                {
                    case Direction.UP:
                        isMoovingUp = isWallUp() ? false : true;
                        if (isMoovingUp)
                        {
                            y -= step1;
                        }
                        break;
                    case Direction.DOWN:
                        isMoovingDown = isWallDown() ? false : true;

                        if (isMoovingDown)
                        {
                            y += step1;
                        }
                        break;
                    case Direction.LEFT:
                        isMoovingLeft = isWallLeft() ? false : true;

                        if (isMoovingLeft)
                        {
                            x -= step1;
                        }
                        break;
                    case Direction.RIGHT:
                        isMoovingRight = isWallRight() ? false : true;

                        if (isMoovingRight)
                        {
                            x += step1;
                        }
                        break;

                }

            }
        }

        public static bool isWallUp()
        {
            bool tmp = Logic.field[(y - step) / step, x / step] == 1;
            return tmp;
        }

        public static bool isWallDown()
        {
            return Logic.field[(y + step) / step, x / step] == 1;
        }

        public static bool isWallRight()
        {
            return Logic.field[y / step, (x + step) / step] == 1;
        }

        public static bool isWallLeft()
        {
            return Logic.field[y / step, (x - step) / step] == 1;
        }




		// попытка создавать врагов разного цвета
		//public Enemy(float enemyY,float enemyX, enemyType t)

		//{
		//	x = enemyX;
		//	y = enemyY;
		//	type = t;

			//switch (type)
			//{
			//	case enemyType.RED:
			//	enemytexture = enemyRed;
			//	break;
			//	case enemyType.BLUE:
			//	enemytexture = enemyBlue;
			//	break;
			//	case enemyType.MAGENTA:
			//	enemytexture = enemyMagenta;
			//	break;
			//}

		//}


		public void Draw(SpriteBatch sb)
			{
			
			sb.DrawSprite(enemyRed, x, y, 30, 30, a, Color.White);
			}



		 
		//public override void Draw(GameTime gameTime, StereoEye stereoEye)
		//{
		//	var sb = Game.GetService<SpriteBatch>();

		//	sb.Begin();

		//	float dx = rand.NextFloat(5, 10);
		//  //  float dy = rand.NextFloat(5, 10);
		//	xRed -= dx * gameTime.ElapsedSec;
		//	//y += dy * gameTime.ElapsedSec;

        

		//	if (isAliveRed)
		//	{
		//		sb.DrawSprite(enemyRed, xRed, yRed, 56, 56, a, Color.White);
		//	}

		//	if (isAliveBlue)
		//	{
		//		sb.DrawSprite(enemyBlue, xBlue, yBlue, 56, 56, a, Color.White);
		//	}
		//	if (isAliveMagenta)
		//	{
		//		sb.DrawSprite(enemyMagenta, xMagenta, yMagenta, 56, 56, a, Color.White);
		//	}

		//	sb.End();

		//	base.Draw(gameTime, stereoEye);
		//}










	}
}
