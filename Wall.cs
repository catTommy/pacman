﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using Fusion;
using Fusion.Audio;
using Fusion.Content;
using Fusion.Graphics;
using Fusion.Input;
using Fusion.Utils;
using System.Threading.Tasks;

namespace Pacman
{
	class Wall 
	{
		public static Texture2D wall
		{
			get;
			set;
		}

		public float x;
		public float y;
		public float a = 0;

		public  void Draw(SpriteBatch sb)
			{
			sb.DrawSprite(wall, x, y, 30, 30, a, Color.White);
			
			}
		public Wall (float wallY, float WallX)
		{
			y = wallY;
			x = WallX;
		}
	}
}

