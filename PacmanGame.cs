﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using Fusion;
using Fusion.Audio;
using Fusion.Content;
using Fusion.Graphics;
using Fusion.Input;
using Fusion.Utils;

namespace Pacman
{
	public class PacmanGame  : Game
	{

		
		
		/// <summary>
		/// Packman constructor
		/// </summary>
		public PacmanGame()
			: base()
		{
			//	root directory for standard x64 C# application
			Parameters.ContentDirectory = @"..\..\..\Content";

			//	enable object tracking :
			Parameters.TrackObjects = true;

			//	enable developer console :
			Parameters.Developer = true;


			//	enable debug graphics device in Debug :
#if DEBUG
				Parameters.UseDebugDevice	=	true;
#endif

			//	add services :
			AddService(new SpriteBatch(this), false, false, 0, 0);
			AddService(new DebugStrings(this), true, true, 9999, 9999);
			AddService(new DebugRender(this), true, true, 9998, 9998);
			AddService(new Logic(this), true, true, 9997, 1);


			//	add here additional services :

			//	load configuration for each service :
			LoadConfiguration();

			//	make configuration saved on exit :
			Exiting += FusionGame_Exiting;
		}


		/// <summary>
		/// Add services :
		/// </summary>
		protected override void Initialize()
		{
			//	initialize services :

			base.Initialize();
		
			

			//	add keyboard handler :
			InputDevice.KeyDown += InputDevice_KeyDown;
		}



		/// <summary>
		/// Handle keys for each demo
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void InputDevice_KeyDown(object sender, Fusion.Input.InputDevice.KeyEventArgs e)
		{
			if (e.Key == Keys.F1)
			{
				ShowEditor();
			}

			if (e.Key == Keys.F5)
			{
				BuildContent();
				Content.Reload<Texture2D>();
			}

			if (e.Key == Keys.F7)
			{
				BuildContent();
				Content.ReloadDescriptors();
			}

			if (e.Key == Keys.F12)
			{
				GraphicsDevice.Screenshot();
			}

			if (e.Key == Keys.Escape)
			{
				Exit();
			}
		}



		/// <summary>
		/// Save configuration on exit.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void FusionGame_Exiting(object sender, EventArgs e)
		{
			SaveConfiguration();
		}



		/// <summary>
		/// 
		/// </summary>
		/// <param name="gameTime"></param>
		protected override void Update(GameTime gameTime)
		{
			var ds = GetService<DebugStrings>();


			//if (GetService<PacmanHero>().y > 0)
			//{
			//	GetService<PacmanHero>().isMoovingUp = true;
			//}
			//else
			//{
			//	GetService<PacmanHero>().isMoovingUp = false;

			//}

			//if (GetService<PacmanHero>().y < 500)
			//{
			//	GetService<PacmanHero>().isMoovingDown = true;
			//}
			//else
			//{
			//	GetService<PacmanHero>().isMoovingDown = false;

			//}

			//if (GetService<PacmanHero>().x > 0)
			//{
			//	GetService<PacmanHero>().isMoovingLeft = true;
			//}
			//else
			//{
			//	GetService<PacmanHero>().isMoovingLeft = false;

			//}

			//if (GetService<PacmanHero>().x <500)
			//{
			//	GetService<PacmanHero>().isMoovingRight = true;
			//}
			//else
			//{
			//	GetService<PacmanHero>().isMoovingRight = false;

			//}




			//if (p.isMoovingUp)
			//{
			//	if (InputDevice.IsKeyDown(Keys.Up))
			//	{
			//		GetService<PacmanHero>().pacman = GetService<Pacman>().pacmanRight;
			//		GetService<PacmanHero>().y -= 10f;
			//		GetService<PacmanHero>().a = 90;
                    
			//	}

			//}

			//if (GetService<PacmanHero>().isMoovingLeft)
			//{
			//	if (InputDevice.IsKeyDown(Keys.Left))
			//	{
			//		GetService<PacmanHero>().a = 0;
			//		GetService<PacmanHero>().pacman = GetService<Pacman>().pacmanLeft;
			//		GetService<PacmanHero>().x -= 10f;
                    
			//	}
			//}


			//if (GetService<PacmanHero>().isMoovingRight)
			//{
			//	if (InputDevice.IsKeyDown(Keys.Right))
			//	{
			//		GetService<PacmanHero>().pacman = GetService<Pacman>().pacmanRight;
			//		GetService<PacmanHero>().x += 10f;
			//		GetService<PacmanHero>().a = 0;
                    
			//	}
			//}

			//	if ((GetService<PacmanHero>().isMoovingDown)&&!(((Math.Abs(GetService<Pacman>().y - GetService<Map>().y))<5) && (Math.Abs((GetService<Pacman>().x - GetService<Map>().x)))<5))
			//	{
			//		if (InputDevice.IsKeyDown(Keys.Down))
			//		{
			//			GetService<PacmanHero>().pacman = GetService<Pacman>().pacmanRight;
			//			GetService<PacmanHero>().y += 10f;
			//			GetService<PacmanHero>().a = -90;
                        
			//		}
			//	}
			//	if (((Math.Abs(GetService<PacmanHero>().y - GetService<Enemy>().yRed))<40) && ((Math.Abs(GetService<Pacman>().x - GetService<Enemy>().xRed))<40)){
			//	  GetService<Pacman>().isAlive = false;
			//	}

			//	if (((Math.Abs(GetService<PacmanHero>().y - GetService<Enemy>().yBlue)) < 40) && ((Math.Abs(GetService<Pacman>().x - GetService<Enemy>().xBlue)) < 40))
			//	{
			//		GetService<PacmanHero>().isAlive = false;
			//	}
			//	if (((Math.Abs(GetService<PacmanHero>().y - GetService<Enemy>().yMagenta)) < 40) && ((Math.Abs(GetService<Pacman>().x - GetService<Enemy>().xMagenta)) < 40))
			//	{
			//		GetService<PacmanHero>().isAlive = false;
			//	}


			//	if ((GetService<Pacman>().y == GetService<Map>().y) && (GetService<Pacman>().x == GetService<Map>().x)){
				
				

			//	}



			ds.Add(Color.Orange, "FPS {0}", gameTime.Fps);
			ds.Add("F1   - show developer console");
			ds.Add("F5   - build content and reload textures");
			ds.Add("F12  - make screenshot");
			ds.Add("ESC  - exit");

			base.Update(gameTime);
		}



		/// <summary>
		/// 
		/// </summary>
		/// <param name="gameTime"></param>
		/// <param name="stereoEye"></param>
		protected override void Draw(GameTime gameTime, StereoEye stereoEye)
		{
			//var sb = GetService<SpriteBatch>();
			//sb.Draw(texture, 0, 0, 56, 56, Color.White);
			
			
			base.Draw(gameTime, stereoEye);
		}
	}
}
