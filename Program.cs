﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pacman
{
	class Program
	{

		[STAThread]
		static void Main(string[] args)
		{
			using (var game = new PacmanGame())
			{
				game.Parameters.TrackObjects = true;
				game.Parameters.Height = 800;
                game.Parameters.Width = 1400;
				game.Run(args);
			}
		}
	}
}
