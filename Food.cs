﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using Fusion;
using Fusion.Audio;
using Fusion.Content;
using Fusion.Graphics;
using Fusion.Input;
using Fusion.Utils;
using System.Threading.Tasks;

namespace Pacman
{
	class Food //: GameService
	{
		public static Texture2D food
		{
			get;
			set;
		}

		public Food (float foodY, float foodX)
		{
		x= foodX;
		y= foodY;
		}

		public float x;
		public float y;
		public float a = 0;

		public  void Draw(SpriteBatch sb)
			{
			sb.DrawSprite(food, x, y, 20, 20, a, Color.White);
			
			}


	}
}
