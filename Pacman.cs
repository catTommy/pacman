﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using Fusion;
using Fusion.Audio;
using Fusion.Content;
using Fusion.Graphics;
using Fusion.Input;
using Fusion.Utils;
using Fusion.UserInterface;
using System.ComponentModel;

namespace Pacman{

	public enum Direction
	{
		UP,
		DOWN,
		LEFT,
		RIGHT,
	}
	
	
	class PacmanHero{ 

		public Texture2D pacman;
        public Texture2D pacmanLeft;
        public Texture2D pacmanRight;

		public int x;
		public int y;
        public int step = 30;


		public float a = 0;
        public bool isMoovingUp = true;
        public bool isMoovingDown = true;
        public bool isMoovingLeft = true;
        public bool isMoovingRight = true;
        public bool isAlive = true;

		public Direction pacmanDirection;

		public PacmanHero()
        {

        }

		public void Moving(InputDevice inputDevice)
		{
			if (inputDevice.IsKeyDown(Keys.Up))
            {
				pacman = pacmanRight;
                pacmanDirection = Direction.UP;
                a = 90;
                if (y > 30 && isMoovingUp)
                {
					y -= step;
					
                    }
				}

			if (inputDevice.IsKeyDown(Keys.Down))
			{
				pacmanDirection = Direction.DOWN;
				pacman = pacmanRight;
                a = -90;
                if (isMoovingDown)
				{
					y += step;
					
                }
                    
			}

			if (inputDevice.IsKeyDown(Keys.Left))
			{
				pacmanDirection = Direction.LEFT;
                pacman = pacmanLeft;
                a = 0;
				if (x > 30 && isMoovingLeft)
                {
					
					x -= step;
                }
			}


			if (inputDevice.IsKeyDown(Keys.Right))
			{
				pacman = pacmanRight;
				pacmanDirection = Direction.RIGHT;
                a = 0;
				if (isMoovingRight)
					{
						x += step;
						
                    }
				}
		}

		public  void Draw(SpriteBatch sb)
		{
			sb.DrawSprite(pacman, x, y, 30, 30, a, Color.White);
		}
	}
}

